[general]
# whether to cache derived blocks
cache_derived   = True
# blocks with very time-consuming calculations can always be cached
# Unix shell-style wildcards are understood
always_cache    = age, temp, mag*, # Ekin, angmom, jcirc, LX

[iontable]
tabledir        = pygad-analysis/ionfiles
#pattern         = lt<z>HM12_i9
#ions            = H I, He II, C III, C IV, O IV, O VI, Ne VIII, Mg II, Si IV
pattern         = lt<z>HM12_i31
ions            = H I, He I, He II, C II, C III, C IV, C V, N IV, N V, N VI, O I, O II, O III, O IV, O V, O VI, O VII, O VIII, Ne IV, Ne VIII, Mg II, Mg X, Al II, Al III, Si II, Si III, Si IV, Si XII, P IV, S VI, Fe II
nH_vals         = -9.0, 0.05, 240
T_vals          =  2.5, 0.05, 140
selfshield      = True
flux_factor     = 1.0

[rules]
# Definition of derived blocks. The rules have to be interpretable by Snap.get,
# with the additional use of the aliases defined in the corresponding section.

# the individual elements are automatically registered as derived arrays
elements    = (Z.T * mass).T                             ; element masses
H	    = (mass- (He+ C+ N+ O+ Ne+ Mg+ Si+ S+ Ca+ Fe))
metallicity = metals/elements.sum(axis=1)   ; metallicity
metals      = elements.sum(axis=1) - (H+He) ; metal mass
alpha_el    = O+C+Ne+Si+Mg+S+Ca             ; alpha element mass
HI          = calc_HI_mass(gas)             ; the HI mass (UVB as in gadget.cfg)
HII         = H - HI                        ; the (remaining) HII mass

# this can be used to overwrite the volume definition in `gadget.cfg` (by
# `vol_def_x`)
dV          = mass / rho

# sub-arrays
x           = pos[:,0]
y           = pos[:,1]
z           = pos[:,2]
vx          = vel[:,0]
vy          = vel[:,1]
vz          = vel[:,2]

# dist(a[,b]) is much faster than sqrt(sum((a[-b])**2, axis=1))
age         = age_from_form(form_time, self)            ; ages (from formation time)
# temperatures
#temp        = calc_temps(u, XH=0.76, ne=ne, subs=self)
temp        = calc_temps(u, XH=H/mass, ne=ne, subs=self)
#temp        = calc_temps(u, XH=H/mass, ne=ne, \
#                         XZ=[(O/mass,15.999), (C/mass,12.011), (Ne/mass,20.180), \
#                             (Fe/mass,55.845), (N/mass,14.007), \
#                             (Si/mass,28.085), (Mg/mass,24.305), (S/mass,32.06), \
#                             (Ca/mass,12.011)], \
#                         subs=self)
r           = dist(pos)                     ; spherical radius
rcyl        = dist(pos[:,:2])               ; cylindrical radius
vrad        = inner1d(pos,vel) / r          ; radial velocities
momentum    = (mass * vel.T).T              ; momentum
angmom      = UnitQty(cross(pos,momentum),pos.units*momentum.units)     ; angular momentum
Ekin        = 0.5*mass*sum(vel**2,axis=1)   ; kinetic energy
# TODO: check if the zero point of the potential is what I expect
Epot        = mass*pot                      ; (Newtonian) potential energy
E           = Ekin+Epot                     ; (Newtonian) total energy

# the angular momentum a particle on a circular orbit with the same energy would have
jcirc       = r * sqrt(2.0*mass*Ekin)
# the parameter jz/jc, where jz is the z-component of the angular momentum and jc=jcirc:
jzjc        = angmom[:,2] / jcirc
vcirc       = sqrt(sum(vel**2,axis=-1) - vrad**2)   ; circular part of the velocities

# magnitudes (corresponding luminosities get derived automatically):
mag         = inter_bc_qty(age,metallicity,'Mbol',units='mag')  ; bolometric magnitudes
mag_u       = inter_bc_qty(age,metallicity,'Umag',units='mag')  ; U-band magnitudes
mag_b       = inter_bc_qty(age,metallicity,'Bmag',units='mag')  ; B-band magnitudes
mag_v       = inter_bc_qty(age,metallicity,'Vmag',units='mag')  ; V-band magnitudes
mag_r       = inter_bc_qty(age,metallicity,'Rmag',units='mag')  ; R-band magnitudes
mag_k       = inter_bc_qty(age,metallicity,'Kmag',units='mag')  ; K-band magnitudes

# X-ray luminosities; only for given redshift; path likely needs to be adjusted...
LX          = calc_x_ray_lum(gas, \
                             lumtable=module_dir+'/../snaps/em.dat', \
                             Zref=0.4, z_table=0.001)

# derived from tracing files:
insitu      = rR200form < 0.1

